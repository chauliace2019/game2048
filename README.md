# game2048
Chauliac Edouard
Carpier Alexis

from random import *

taille_max_case=4
THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}


def create_grid(taille_max_case):
    '''Creation d'une grille de taille donnée'''
    game_grid = []
    for k in range(taille_max_case):
        game_grid.append(taille_max_case*[0])
    return game_grid


def grid_add_new_tile(game_grid):
    '''ajout d'une nouvelle tuile a endroit donne avec probabilite de 0.9 que ce soit un 2 et proba de 0.1 que ce soit un 4'''
    x,y=get_new_position(game_grid)
    proba=random()
    if proba <=0.9:
        game_grid[x][y]=2
    else:
        game_grid[x][y]=4
    return game_grid


#def get_value_new_tile():
 #   new_grid=grid_add_new_tile(create_grid(taille_max_case))
  #  if new_grid[0][0]==2 or 4:
   #     return new_grid[0][0]
    
def get_all_tiles(game_grid):
    '''on parcourt la grille pour obtenir toutes les valeurs des tuiles de la grille'''
    result=[]
    for ligne in game_grid:
        for element in ligne:
            if element==' ':
                result.append(0)
            else:
                result.append(element)
    return result


def get_empty_tiles_positions(game_grid):
    '''on parcourt la grille et on releve les positions de toutes les tuiles vides de la grille'''
    result=[]
    for nb_ligne in range(len(game_grid)):
        for nb_colonne in range(len(game_grid[0])):
            if game_grid[nb_ligne][nb_colonne] == 0 or game_grid[nb_ligne][nb_colonne] == ' ':
                result.append((nb_ligne,nb_colonne))
    return result

def get_new_position(game_grid):
    '''on choisit aletoirement une position pour la nouvelle tuuile parmi toutes les positions libres disponibles'''
    empty_tiles_position = get_empty_tiles_positions(game_grid)
    return choice(empty_tiles_position)

def grid_get_value(grid,x,y):
    if grid[x][y] == 0 or ' ':
        return 0
    return grid[x][y]


def init_game(size):
    game_grid=grid_add_new_tile(create_grid(size))      #on ajoute une nouvelle tuile à une grille créée de taille donnée
    game_grid=grid_add_new_tile(game_grid)              #on ajoute une deuxieme tuile à la grille precedente
    return game_grid


#Fonctionnalite2

def grid_to_string(game_grid,nb_case):
    result=''''''                                      #str qui nous servira a faire apparaitre la grille finale
    grid = '''
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
    '''
    list_tiles = get_all_tiles(game_grid)               #on releve les valeurs de toutes les tuiles de la grille
    if list_tiles==[]:                                  #si cette liste est vide on renvoie la grille grid
        return grid
    else:
        grid = list(grid)                               #on transforme grid en liste pour pouvoir le modifier
        list_tiles.reverse()                            #on inverse la liste des valeurs des tuiles de la grille pour utiliser pop
        compteur = 0                                    #on cree un compteur pour compter '|'
        for value in range(len(grid)):
            if grid[value] == '|' and list_tiles != [] and compteur !=nb_case:      #si on rencontre '|' et que le compteur ne depasse pas le nb de case d'une ligne de la grille
                compteur +=1                                                        #on augmente le compteur de 1
                grid[value + 2] = list_tiles.pop()                                  #on affecte la valeur de la tuile correspondante 
                if grid [value+2] == 0:
                    grid[value+2] = ' '                                             #si la tuile est nulle on laisse un espace vide ' '
            elif grid[value] == '|' and compteur ==nb_case:
                compteur=0
                continue   
    for element in grid:                                                        # on repasse la liste grid en chaine de caractere pour afficher la grille finale
        if type(element)==int:
            element= str(element)
        result += element
    return result
    
def long_value_with_theme(grid_game,THEMES):
    '''on determine la longueur maximale de case suivant le theme choisi et la grille'''
    list_values = get_all_tiles(grid_game)              #on recupere les valeurs de toutes les tuiles
    list_tiles = []
    max_length = 0
    for key in list_values:
         list_tiles.append(THEMES[key])                 #on recupere les valeurs des cles du dictionnaire corespondant au theme chois
    for element in list_tiles:
        if len(element) > max_length:
            max_length = len(element)
    return max_length                                   #on renvoie la longuer maximale de case de la grille suivant le theme considere

def grid_to_string_with_size_and_theme(game_grid,THEMES,size):
    taille_max_case = long_value_with_theme(game_grid,THEMES)
    result = ''''''
    result_list = ['\n']+size*((size*(taille_max_case)+size+1)*['=']+['\n'] +size*(['|']+taille_max_case*[' '])+['|','\n'])+['=']+(size*(taille_max_case)+size)*['=']+['\n']   #on cree la grille graphique sous forme de liste en fonction du nombre de case et de la taille maximale des cases
    list_tiles = get_all_tiles(game_grid)
    if list_tiles==[]:                                      #on utilise le meme procede que grid_to_string pour remplir cette liste
        for element in result_list:
            result += element
        return result
    else:
        list_tiles.reverse()
        compteur = 0
        for value in range(len(result_list)):
            if result_list[value] == '|' and list_tiles != [] and compteur !=size:
                compteur +=1
                tile = list_tiles.pop()
                tile = THEMES[tile]                                     #on prend en compte le theme choisi pour renvoyer les donnees correspondant aux nombres ecrits dans game_grid initialement
                for char in range(len(tile)):
                    result_list[value + char +1] = tile[char]
                if result_list [value+1] == 0:
                    result_list[value+1] = ' '
            elif result_list[value] == '|' and compteur ==size:
                compteur=0
                continue   
    for element in result_list:                                         #on transforme la liste resultat en chaine de caractere pour obtenir la grille et les tuiles en console
        if type(element)==int:
            element= str(element)
        result += element
    return result



#Fonctionnalite4

def move_row_left(ligne):                                   #on cree recursivement une fonction pour deplacer les tuiles ici a gauche
    def deplacement(ligne):
        if len(ligne)<=1:                                   #critere d'arret de la fonction
            return ligne
        elif ligne[0]==0 and ligne[0]!=ligne[1]:            #si on a un 0 tout a gauche de la liste
            ligne = ligne[1:] + [0]                         #on le deplace tout a droite de la lsite
            return (deplacement(ligne))
        elif ligne[0]==0 and ligne[0]==ligne[1]:            #si on a deux 0 à la suite au debut
            if ligne != ligne[1:]+[0]:                      #critere pour eviter des boucles infinies 
                return deplacement(ligne[1:]+[0])
            else:
                return ligne
        elif ligne[0] !=0 and ligne[0] != ligne[1]:         #si on a deux nombres differents de 0 et non identiques qui se suivent
            return [ligne[0]] + deplacement(ligne[1:])
        else:
            return [ligne[0]] + deplacement(ligne[1:])      #on a deux nombres differents de 0 identiques qui se suivent
    ligne = deplacement(ligne)
    for length in range (len(ligne)-1):                     #on s'occupe de la fusion des possibles doublons de nombres cote a cote
        if ligne[length] == ligne[length+1]:
           ligne[length] = 2*ligne[length]
           ligne[length+1] = 0
           ligne =  deplacement(ligne)                      #on effectue un deplacement pour gerer cette fusion de case
    return ligne

def move_row_right(ligne):                                  #on effectue le meme procede que move_row_left sauf qu'on part de la fin de la liste pour remonter vers le debut
    def deplacement(ligne):
        if len(ligne)<=1:
            return ligne
        elif ligne[-1]==0 and ligne[-1]!=ligne[-2]:
            ligne = [0] + ligne[:-1]
            return (deplacement(ligne))
        elif ligne[-1]==0 and ligne[-1]==ligne[-1]:
            if ligne != [0]+ ligne[:-1]:
                return deplacement([0]+ ligne[:-1])
            else:
                return ligne
        elif ligne[-1] !=0 and ligne[-1] != ligne[-1]:
            return deplacement(ligne[:-1]) + [ligne[-1]]
        else:
            return deplacement(ligne[:-1]) + [ligne[-1]]
    ligne = deplacement(ligne)
    for length in range (len(ligne)-1,1,-1):
        if ligne[length] == ligne[length-1]:
           ligne[length] = 2*ligne[length]
           ligne[length-1] = 0
           ligne =  deplacement(ligne)
           print(ligne)
    return ligne



def move_grid(game_grid,direction):                     #on gere les deplacements de toutes les lignes de la grille
    result=[]
    if direction in ["left","g"]:                       #si le deplacement se fait a gauche
        for element in game_grid:
            element = move_row_left(element)            #on bouge tous les elements des listes de la grille a gauche
            result.append(element)
    elif direction in ["right","d"]:                    #si le deplacement est a droite
        for element in game_grid:
            element = move_row_right(element)           #on bouge tous les elements des listes a droite
            result.append(element)
    elif direction in ["up","h"]:                                                                           #si le deplacement se fait en haut
        liste_aux =[[game_grid[k][i] for k in range(len(game_grid))] for i in range(len(game_grid))]        #on transforme game_grid de facon à intervertir ligne et colonne
        for element in liste_aux:                                                                           #on effectue une transformation a gauche de ces listes
            element = move_row_left(element)
            result.append(element)
        result = [[result[k][i] for k in range(len(liste_aux))] for i in range(len(liste_aux))]             #on reechange lignes et colonnes

    elif direction in ["down","b"]:                                                                         #si le deplacement se fait en bas
        liste_aux =[[game_grid[k][i] for k in range(len(game_grid))] for i in range(len(game_grid))]        #on echange lignes et colonnes
        for element in liste_aux:
            element = move_row_right(element)                                                               #on effectue une transformation a droite de ces listes
            result.append(element)
        result = [[result[k][i] for k in range(len(liste_aux))] for i in range(len(liste_aux))]             #on reechange listes et colonnes
    return result

#Fonctionnalite5

def is_full_grid(grid):             
    for element in grid:        #si un element de grid est 0 alors la liste n'est pas remplie
        if 0 in element:
            return False
    return True

def move_possible(grid):                    
    result = [False,False,False,False]
    liste_directions = ["g","d","h","b"]
    for direction in liste_directions:
        grid_aux = move_grid(grid,direction)            #on teste pour chaque ligne et colonne de grid les 4 deplacements
        for length in range(len(grid_aux)):
            if grid_aux[length] != grid[length]:
                result[length] = True                   #si un deplacement est possible on remplace False par True
    return result


def is_game_over_grid(grid):
    if is_full_grid(grid):                              #si la liste est pleine et qu'il n'y a plus de deplacement possible
        if move_possible == [False,False,False,False]:
            return "Game over"                          #le jeu est fini

def get_grid_tile_max(grid):
    list_tiles = get_all_tiles(grid)                    #on trouve la tuile maximale de la grille
    return max(list_tiles)

#Fonctionnalite 6

from textual_2048 import read_size_grid,read_theme_grid,read_player_command


def random_play():                                              #on fait jouer l'ordinateur au jeu 
    game_grid = init_game(taille_max_case)
    directions =["left","right","up","down"]
    print(grid_to_string_with_size_and_theme(game_grid,THEMES["0"],4))
    while is_game_over_grid(game_grid) != "Game over" :                 #tant que la grille n'est pas pleine et qu'un deplacement est possible
        game_grid = move_grid(game_grid,choice(directions))             #choix aleatoire d'une direction
        if not is_full_grid(game_grid):
            game_grid = grid_add_new_tile(game_grid)
            print(grid_to_string_with_size_and_theme(game_grid,THEMES["0"],4))  #affichage nouvelle grille apres ajout tuile et deplacement
        else:
            break
    if get_grid_tile_max(game_grid) >=2048:                                 #si jeu est fini on regarde si la tuile maximale est plus grande que 2048
        return "Vous avez gagné !"
    else:
        return "Vous avez perdu !"




def read_player_command():                                                                  #demande direction du joueur
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)): ")
    if move not in ["g","d","h","b"]:
        print ("Erreur de commandes !")
        return read_player_command()
    else:
        return move

def read_size_grid():                                               #demande taille de la grille au joueur
    size = int(input("Entrez la taille de votre grille: "))
    return size

def read_theme_grid():                                              #demande du theme de la grille au joueur
     theme = input("Entrez le numéro du thème de votre grille: ")
     return theme

def ask_and_read_grid_size():
    size = read_size_grid()
    return size

def ask_and_read_grid_theme():
    theme = read_theme_grid()
    return theme

def game_play():
    size,num_theme = ask_and_read_grid_size(),ask_and_read_grid_theme()                 #on code une fonction similaire a random_play() sauf qu'on ajoute les choix utilisateurs pour choisir la direction la taille et le theme
    num_theme = str(num_theme)
    theme = THEMES[num_theme]
    game_grid = init_game(size)
    print(grid_to_string_with_size_and_theme(game_grid,theme,size))
    while is_game_over_grid(game_grid) != "Game over":
        move = read_player_command()
        print(move)
        game_grid = move_grid(game_grid,move)
        if not is_full_grid(game_grid):
            game_grid = grid_add_new_tile(game_grid)
            print (grid_to_string_with_size_and_theme(game_grid,theme,size))
        else:
            break
    if get_grid_tile_max >= theme[2048]:
        return "Vous avez gagné !"
    else:
        return "Vous avez perdu !"

if __name__ == '__main__':
    game_play()
    exit(1) 
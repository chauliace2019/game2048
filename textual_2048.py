#FONCTIONNALITE3

def read_player_command():
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)): ")
    if move not in ["g","d","h","b"]:
        print ("Erreur de commandes !")
        return read_player_command()
    else:
        return move

def read_size_grid():
    size = int(input("Entrez la taille de votre grille: "))
    return size

def read_theme_grid():
     theme = input("Entrez le numéro du thème de votre grille: ")
     return theme


